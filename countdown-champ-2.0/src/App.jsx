import React, { Component } from 'react';
import Clock from './Clock';
import './App.css';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deadline: 'November 12, 2017',
            newDeadline: ''
        }
    }

    changeDeadline() {
        if (Date.parse(this.state.newDeadline) - Date.parse(new Date()) > 0) {
            this.setState({ deadline: this.state.newDeadline }, function () {
                console.log(this.state);
            });
            console.log(this.state);
        }
        else {
        }
    }

    render() {
        return (
            <div>
                <div>
                    Countdown to {this.state.deadline}
                </div>
                <Clock deadline={this.state.deadline} />

                <input
                    type="text"
                    placeholder="new deadline"
                    onChange={event => this.setState({ newDeadline: event.target.value })}
                />
                <button className="btn btn-success" onClick={event => this.changeDeadline()}>
                    Submit
                </button>

            </div>
        )
    }
}

export default App;