import React, { Component } from 'react';

class Clock extends Component {
    constructor(props) {
        super(props);
        this.state = {
            days: 0,
            hours: 0,
            minutes: 0,
            seconds: 0
        }
        
    }

    leadingZero(num) {
        return num < 10 ? '0' + num : num;
    }

    getDateTimes(deadline) {
        let millis = (Date.parse(deadline) - Date.parse(new Date())) / 1000;

        let seconds = Math.floor(millis % 60);
        let minutes = Math.floor((millis / 60) % 60);
        let hours = Math.floor((millis / 3600) % 24);
        let days = Math.floor(millis / (3600 * 24));
        console.log(this.props.millis);
        this.setState({ days, hours, minutes, seconds });
    }

    componentWillMount() {
        
    }

    componentDidMount() {
        setInterval(() => this.getDateTimes(this.props.deadline), 1000);
    }

    render() {
        return (
            <div>
                <span>{this.leadingZero(this.state.days)} days</span>
                <span>{this.leadingZero(this.state.hours)} hours</span>
                <span>{this.leadingZero(this.state.minutes)} minutes</span>
                <span>{this.leadingZero(this.state.seconds)} seconds</span>
            </div>
        )
    }
}

export default Clock;